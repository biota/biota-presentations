# Command to generate PDF presentations
TEX=lualatex -output-directory=build


# Target to build all presentations
all: kickstart


# Target to build kickstart.pdf
kickstart: kickstart.tex
	mkdir -p build
	$(TEX) $<
	$(TEX) $<


# Target to clean up generated PDF files
clean:
	rm -rf build
